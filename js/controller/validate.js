// hợp lệ => true
var showMessage = function (id, message) {
  document.getElementById(id).innerHTML = `
  <strong> ${message}</strong>
  `;
};
var kiemtraTrung = function (maSV, dssv) {
  var index = dssv.findIndex(function (item) {
    return maSV == item.ma;
  });
  if (index == -1) {
    showMessage("spanMaSV", "");
    return true;
  } else {
    showMessage("spanMaSV", "Mã Sinh Viên Không Hợp Lệ");
    return false;
  }
  //spanMaSV;
};
var kiemtraRong = function (idErr, value) {
  if (value.length == 0 || !value) {
    showMessage(idErr, "Trường Này Không được Trống");
    return false;
  } else {
    showMessage(idErr, "");
    return true;
  }
};
var kiemtraEmail = function (email) {
  const re =
    /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
  if (re.test(email)) {
    showMessage("spanEmailSV", "");
    return true;
  } else {
    showMessage("spanEmailSV", "Email khong hop le");
    return false;
  }
};
//sass
