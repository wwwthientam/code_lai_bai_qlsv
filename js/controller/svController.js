function renderDSSV(svArr) {
  //render len layout
  var contentHTML = "";
  for (var i = 0; i < svArr.length; i++) {
    var a = svArr[i];
    //a là 1 item trong array dssv
    var contentTr = `<tr>
<td> ${a.ma}</td>
<td> ${a.ten}</td>
<td> ${a.email}</td>
<td> ${a.tinhDTB()}</td>
<td>
<button onclick="xoaSV(${a.ma})" class="btn btn-danger">Xóa</button>
<button onclick="suaSV(${a.ma})" class="btn btn-primary">Sửa</button>
</td>
</tr>
    `;
    contentHTML = contentHTML + contentTr;
  }
  document.getElementById("tbodySinhVien").innerHTML = contentHTML;
}
//local storage: lưu trữ

//JSON stringtify, JSON parse: convert data
//lay thong tin tu form
function layThongTinTuForm() {
  var ma = document.getElementById("txtMaSV").value;
  var ten = document.getElementById("txtTenSV").value;
  var email = document.getElementById("txtEmail").value;
  var matkhau = document.getElementById("txtPass").value;
  var toan = document.getElementById("txtDiemToan").value * 1;
  var ly = document.getElementById("txtDiemLy").value * 1;
  var hoa = document.getElementById("txtDiemHoa").value * 1;
  var sv = {
    ma: ma,
    ten: ten,
    email: email,
    pass: matkhau,
    toan: toan,
    ly: ly,
    hoa: hoa,
    tinhDTB: function (sv) {
      return (this.toan + this.ly + this.hoa) / 3;
    },
  };
  return new SinhVien(ma, ten, email, matkhau, toan, ly, hoa);
}
function showThongTinLenForm(sv) {
  document.getElementById("txtMaSV").value = sv.ma;
  document.getElementById("txtTenSV").value = sv.ten;
  document.getElementById("txtEmail").value = sv.email;
  document.getElementById("txtPass").value = sv.matKhau;
  document.getElementById("txtDiemToan").value = sv.toan;
  document.getElementById("txtDiemLy").value = sv.ly;
  document.getElementById("txtDiemHoa").value = sv.hoa;
}
