var dssv = [];
//lấy json lên khi user load trang
var dataJson = localStorage.getItem("DSSV_LOCAL");
//convert ngược lại từ json về array
if (dataJson != null) {
  var dataArr = JSON.parse(dataJson);
  for (var i = 0; i < dataArr.length; i++) {
    var item = dataArr[i];
    var sv = new SinhVien(
      item.ma,
      item.ten,
      item.email,
      item.matkhau,
      item.toan,
      item.ly,
      item.hoa
    );
    dssv.push(sv);
  }
  // //JSON.parse(dataJson) sẽ return về null hoặc array
  console.log(dssv);
  renderDSSV(dssv);
}
function saveData(dssv) {
  localStorage.setItem("DSSV_LOCAL", JSON.stringify(dssv));
}
function themSinhVien() {
  //luu lai
  var sv = layThongTinTuForm();
  // maSV
  var isValid = kiemtraTrung(sv.ma, dssv);
  //tenSV
  isValid = isValid & kiemtraRong("spanTenSV", sv.ten);
  //emailSV
  isValid =
    isValid & kiemtraRong("spanEmailSV", sv.email) && kiemtraEmail(sv.email);
  //matkhauSV
  isValid = isValid & kiemtraRong("spanMatKhau", sv.matkhau);
  //diemToan
  isValid = isValid & kiemtraRong("spanToan", sv.toan);
  if (isValid) {
    dssv.push(sv);
    //convert array dssv thanh json
    var dataJson = JSON.stringify(dssv);
    console.log(dataJson);
    //SET GET MOVE
    // lưu json xuống
    localStorage.setItem("DSSV_LOCAL", dataJson);
    //render len layout
    renderDSSV(dssv);
  }
}
function xoaSV(id) {
  //tìm vị trí
  var viTri = -1;
  for (var i = 0; i < dssv.length; i++) {
    var sv = dssv[i];
    if (sv.ma == id) {
      viTri = i;
    }
  }
  dssv.splice(viTri, 1);
  saveData(dssv);
  renderDSSV(dssv);
}
function suaSV(id) {
  console.log("id", id);
  var viTri = dssv.findIndex(function (item) {
    console.log("hello");
    return item.ma == id;
  });
  console.log(viTri);
  // if (viTri==-1) return;
  if (viTri != -1) {
    //chặn user sửa mã sv
    document.getElementById("txtMaSV").disabled = true;
    showThongTinLenForm(dssv[viTri]);
  }
}
function capNhatSinhVien() {
  document.getElementById("txtMaSV").disabled = false;
  //lay thong tin tu form
  var sv = layThongTinTuForm();
  var viTri = dssv.findIndex(function (item) {
    return item.ma == sv.ma;
  });
  console.log(viTri);
  if (viTri !== -1) {
    dssv[viTri] = sv;
    saveData(dssv);
    renderDSSV(dssv);
    resetForm();
  }
}
function resetForm() {
  document.getElementById("formQLSV").reset();
  document.getElementById("txtMaSV").disabled = false;
}
